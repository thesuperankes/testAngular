import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StudentDashboardComponent } from './components/student-dashboard/student-dashboard.component';
import { TeacherDashboardComponent } from './components/teacher-dashboard/teacher-dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { GeneralinfoComponent } from './components/admin/generalinfo/generalinfo.component';

import { StudentsService } from './services/students.service';
import { SubjectsService } from './services/subjects.service';
import { SubjectPerStudentService } from './services/subject-per-student.service';
import { LoginService } from './services/login.service';
import { TeachersService } from './services/teachers.service';

import { app_routing } from './app-routing.module';
import { CheckStudentComponent } from './components/admin/check-student/check-student.component';
import { CheckTeacherComponent } from './components/admin/check-teacher/check-teacher.component';
import { CheckSubjectComponent } from './components/admin/check-subject/check-subject.component';






@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    StudentDashboardComponent,
    LoginComponent,
    TeacherDashboardComponent,
    AdminComponent,
    CheckStudentComponent,
    CheckTeacherComponent,
    CheckSubjectComponent,
    GeneralinfoComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    app_routing,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    StudentsService,
    SubjectsService,
    TeachersService,
    SubjectPerStudentService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
