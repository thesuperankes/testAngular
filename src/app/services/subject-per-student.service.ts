import { Injectable } from '@angular/core';
import { Subjectsbystudent } from '../entities/subjectsbystudent';
import { Students } from '../entities/students';
import { Subjects } from '../entities/subjects';

@Injectable({
  providedIn: 'root'
})
export class SubjectPerStudentService {

  initialData:Subjectsbystudent[] =[{
    studentId : 1,
    subjectsId : [1,3]
  },{
    studentId:2,
    subjectsId:[2,3]
  }];

  constructor() { }

  getSubjectsByStudentId(id:number){
    let filter = this.initialData.filter(function(e){
      return e.studentId == id;
    });

    return filter[0];
  }
  addSubjectsByStudent(studentId:number, subjectId:number){
    
    let index = this.initialData.map(function(e){return e.studentId = studentId}).indexOf(studentId);
    this.initialData[index].subjectsId.push(subjectId);

    let filter = this.initialData.filter(function(e){
      return e.studentId == studentId
    });

    return filter[0];
  }

  deleteSubjectsByStudent(studentId:number, subjectId:number){
    let index = this.initialData.map(function(e){return e.studentId = studentId}).indexOf(studentId);
    let indexSubject = this.initialData[index].subjectsId.indexOf(subjectId);
    if(indexSubject >= 0){
      this.initialData[index].subjectsId.splice(indexSubject,1);
      return this.initialData;
    }
    return this.initialData;
  }
  
}

