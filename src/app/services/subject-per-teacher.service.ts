import { Injectable } from '@angular/core';
import { Subjectsbyteacher } from '../entities/subjectsbyteacher';

@Injectable({
  providedIn: 'root'
})
export class SubjectPerTeacherService {

  
  initialData:Subjectsbyteacher[] =[{
    teacherid : 1,
    subjectid : [2,1]
  },{
    teacherid:2,
    subjectid:[1]
  },{
    teacherid:3,
    subjectid:[3]
  }];


  constructor() { }

  getSubjectsByTeacherId(id:number){
    let filter = this.initialData.filter(function(e){
      return e.teacherid == id;
    });

    return filter[0];
  }
  addSubjectsByTeacherId(teacherId:number, subjectId:number){
    
    let index = this.initialData.map(function(e){return e.teacherid = teacherId}).indexOf(teacherId);
    console.log(index);
    this.initialData[index].subjectid.push(subjectId);

    let filter = this.initialData.filter(function(e){
      return e.teacherid == teacherId
    });

    return filter[0];
  }

  deleteSubjectsByTeacherId(teacherId:number, subjectId:number){
    let index = this.initialData.map(function(e){return e.teacherid = teacherId}).indexOf(teacherId);
    let indexSubject = this.initialData[index].subjectid.indexOf(subjectId);
    if(indexSubject >= 0){
      this.initialData[index].subjectid.splice(indexSubject,1);
      return this.initialData;
    }
    return this.initialData;
  }
}
