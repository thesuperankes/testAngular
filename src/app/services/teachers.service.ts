import { Injectable } from '@angular/core';
import { Teachers } from '../entities/teachers';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {

  teachers:Teachers[] = [{
    id:1,
    name:'Camilo',
    lastname:'Hernandez',
    email:'teachercami@miraflores.com',
    password:'123456'
  },{
    id:2,
    name:'Andy',
    lastname:'gomez',
    email:'elprofe@miraflores.com',
    password:'123456'
  },{
    id:3,
    name:'Laura',
    lastname:'Lopez',
    email:'lauLopez@miraflores.com',
    password:'123456'
  }]

  constructor() { }

  getAllTeachers(){
    return JSON.stringify(this.teachers);
  }

  loginTeachers(email:string,password:string){
    let teachersFilter = this.teachers.filter(function(e){
      return e.email == email && e.password == password;
    });
    console.log(teachersFilter);
    return teachersFilter[0];
  }

  getTeachersById(id:number){
    let teachersFilter = this.teachers.filter(function(e){
      return e.id == id
    });

    if(teachersFilter.length > 0){
      teachersFilter[0].password = null;
      return teachersFilter[0];
    }
  }

  addTeachers(body:{id:number;name:string,lastname:string,email:string;password:string;}){
    if(this.teachers.length > 0){
      this.teachers.push(body);
      return this.teachers;
    }else{
      body.id = 1;
      this.teachers.push(body);
    }
  }

  deleteTeachers(id:number){
    this.teachers.splice(id - 1,1);
    return this.teachers;
  }

}
