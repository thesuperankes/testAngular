import { Injectable } from '@angular/core';
import { Subjects } from '../entities/subjects';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {
  
  constructor() { }

  subjects:Subjects[] = [{
    id:1,
    title:'Matematicas',
    description:'Aprenderas a realizar funciones y algoritmos'
  },{
    id:2,
    title:'Ingles',
    description:'Aprende a traducir, hablar, escuchar en el idioma mas utilizado globalmente'
  },{
    id:3,
    title:'Programacion',
    description:'Aprende un lenguaje de programacion para el desarrollo de sistemas de software'
  }]

  getAllSubjects(){
    return JSON.stringify(this.subjects);
  }
  getSubjectsById(id:number){
    let subjectsFilter = this.subjects.filter(function(e){
      return e.id == id
    });

    if(subjectsFilter.length > 0){
      
      return subjectsFilter[0];
    }
  }

  addSubjects(body:{id:number,title:string,description:string}){
    if(this.subjects.length > 0){
      this.subjects.push(body);
      return this.subjects;
    }else{
      body.id = 1;
      this.subjects.push(body);
    }
  }

  deleteSubjects(id:number){
    this.subjects.splice(id -1 ,1);
    return this.subjects;
  }
    

}
