import { Injectable } from '@angular/core';
import { StudentsService } from './students.service'
import { TeachersService } from './teachers.service'
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private _studentService:StudentsService, private _teacherService:TeachersService, private router:Router) { }

  isStudent:boolean;
  isTeacher:boolean;
  students;
  teachers;

  checkLogin(email:string,password:string){
    this.students = this._studentService.loginStudents(email,password)
    this.teachers = this._teacherService.loginTeachers(email,password)

    if(this.students){
      localStorage.setItem("isStudent","5a7e5ed9-6107-44c4-9a6e-900d158450ef");
      localStorage.setItem("Student",JSON.stringify(this.students));
      this.router.navigateByUrl("/home")
      return this.isStudent = true;
    }else if(this.teachers && localStorage.getItem("isStudent") == null){
      localStorage.setItem("isTeacher","6e8bd12b-8431-4b3f-b953-15782a3409c6");
      localStorage.setItem("Teacher",JSON.stringify(this.teachers));
      this.router.navigateByUrl("/home")
      return this.isTeacher = true;
    }

    if(email == "admin@admin.com" && password == "123456"){
      localStorage.setItem("isAdmin","0ce860b7-e7cc-4f28-a2e1-91b02f037b1c")
      this.router.navigateByUrl("/admin");
    }

    ;
  }

  logOut(){
    localStorage.removeItem("isStudent");
    localStorage.removeItem("isTeacher");
    localStorage.removeItem("Student");
    localStorage.removeItem("Teacher");
  }
}


