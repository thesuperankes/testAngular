import { Injectable } from '@angular/core';
import { Students } from '../entities/students';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor() { }
  valueMax:number = 0;

  students:Students[] = [{
    id:1,
    name:'Andres',
    lastname:'Lopez',
    email:'lopezavellaneda@hotmail.es',
    password:'123456'
  },{
    id:2,
    name:'Pepito',
    lastname:'perez',
    email:'pepito.perez@hotmail.com',
    password:'123456'
  }]

  getAllStudents(){
    return JSON.stringify(this.students);
  }

  loginStudents(email:string,password:string){
    let filter = this.students.filter(function(e){
      return e.email == email && e.password == password
    });

    return filter[0];
  }

  getStudentById(id:number){
    let studentFilter = this.students.filter(function(e){
      return e.id == id
    });

    if(studentFilter.length > 0){
      studentFilter[0].password = null;
      return studentFilter[0];
    }
  }

  addStudent(body:{id:number;name:string,lastname:string,email:string;password:string;}){
    if(this.students.length > 0){
      this.students.push(body);
      return this.students;
    }else{
      body.id = 1;
      this.students.push(body);
    }
  }

  deleteStudent(id:number){
    this.students.splice(id - 1,1);
    return this.students;
  }



}
