import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: []
})
export class NavbarComponent implements OnInit {
  isStudent = false;
  isTeacher = false;
  constructor(private _login:LoginService, private router:Router) { }

  ngOnInit() {
    if(localStorage.getItem("isStudent") === "5a7e5ed9-6107-44c4-9a6e-900d158450ef"){
      this.isStudent = true; 
    }else if(localStorage.getItem("isTeacher") === "6e8bd12b-8431-4b3f-b953-15782a3409c6"){
      this.isTeacher = true; 
    }
  }

  logOut(){
    this._login.logOut();
    this.isStudent = false;
    this.isTeacher = false;
    this.router.navigateByUrl("/login");
  }

  logIn(){
    this.router.navigateByUrl("/login");
  }

}


