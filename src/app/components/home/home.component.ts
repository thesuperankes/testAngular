import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../../services/students.service';
import { Students } from '../../entities/students';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: []
})
export class HomeComponent implements OnInit {
  title:string;
  message:string;

  student:Students;
  students:Students[];

  constructor(private _studentService:StudentsService) { }
  
  

  ngOnInit() {
    
  }

}