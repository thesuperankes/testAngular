import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../../services/students.service';
import { SubjectsService } from '../../services/subjects.service';
import { SubjectPerStudentService } from '../../services/subject-per-student.service';
import { Students } from '../../entities/students';
import { Subjects } from '../../entities/subjects';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student-dashboard.component.html',
  styleUrls: []
})
export class StudentDashboardComponent implements OnInit {

  constructor(private form: FormBuilder,private _studentService:StudentsService, private _subjectStudent:SubjectPerStudentService,private _subjectService:SubjectsService, private router:Router,private modalService: NgbModal) {}
  
  studensSubjects:Subjects[] = [];
  subjectsId:number[];
  student:Students;
  isLogged:string;
  closeResult: string;
  subjectForm:FormGroup;

  ngOnInit() {
    this.checkLogin();
    
  }

  checkLogin(){
    this.isLogged = localStorage.getItem("isStudent");
    console.log(this.isLogged);
    if(this.isLogged === null || this.isLogged === undefined){
      this.router.navigateByUrl("#/login");
    }

    this.student = JSON.parse(localStorage.getItem("Student"));
    this.subjectsId = this._subjectStudent.getSubjectsByStudentId(this.student.id).subjectsId;
    
    for(let item of this.subjectsId){
      this.studensSubjects.push(this._subjectService.getSubjectsById(item));
    }
    
    console.log(this.studensSubjects);
  }

  getRandomNumber(){
    return Math.floor(Math.random() * 10) + 1;
  }

}
