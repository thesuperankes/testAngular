import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;

  constructor(private form: FormBuilder, private _loginService:LoginService,private router:Router,private _toastrService:ToastrService) { }

  ngOnInit() {
    this.createForm();
  }

  checkLogin(){
    let isLogged = this._loginService.checkLogin(this.loginForm.value.email,this.loginForm.value.password)

    if(isLogged == true){
      this.router.navigateByUrl("/home");
    }else if(localStorage.getItem("isAdmin") == null){
      this._toastrService.error('Usuario o contraseña incorrectos','El ingreso falló');
    }
  }

  createForm(){
    this.loginForm = this.form.group({
      email:['',Validators.required],
      password:['',Validators.required]
    });
  }

}
