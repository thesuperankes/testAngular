import { Component, OnInit } from '@angular/core';
import { TeachersService } from '../../../services/teachers.service';
import { Teachers } from '../../../entities/teachers';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-check-teacher',
  templateUrl: './check-teacher.component.html',
  styleUrls: []
})
export class CheckTeacherComponent implements OnInit {

  constructor(private form: FormBuilder,private _teachersServices:TeachersService, private router:Router,private _toastrService:ToastrService) { }
  
  teachers:Teachers[];
  teacherForm:FormGroup;

  ngOnInit() {
    if(localStorage.getItem("isStudent") || localStorage.getItem("isTeacher") || localStorage.getItem("isAdmin") == null){
      this.router.navigateByUrl("/home");
    }
    this.teachers = JSON.parse(this._teachersServices.getAllTeachers());
    this.createForm();
  }

  createForm(){
    this.teacherForm = this.form.group({
      id:['',Validators.required],
      name:['',Validators.required],
      lastname:['',Validators.required],
      email:['',Validators.email],
      password:['',Validators.required]
    });
  }

  addTeacher(){
    if(this.teacherForm.valid){
      this.teachers = this._teachersServices.addTeachers(this.teacherForm.value);
      this._toastrService.success('El profesor fue agregado correctamente','Profesor agregado correctamente');
    }else{
      this._toastrService.error('Revisa que los tipos de los campos concuerden con los requeridos','Formulario invalido');
    }
  }

  deleteTeacher(id){
    this.teachers = this._teachersServices.deleteTeachers(id);
  }
  
}
