import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private router:Router) { }

  checkstudents:boolean;
  checkteachers:boolean;
  checksubjects:boolean;
  checkreport:boolean;

  ngOnInit() {
    this.checkreport = true;

    if(localStorage.getItem("isStudent") || localStorage.getItem("isTeacher") || localStorage.getItem("isAdmin") == null){
      this.router.navigateByUrl("/home");
    }
  }

  checkStudents(){
    this.checkteachers = false;
    this.checksubjects = false;
    this.checkreport = false;
    this.checkstudents = true;
  }

  checkTeachers(){
    this.checkstudents = false;
    this.checksubjects = false;
    this.checkreport = false;
    this.checkteachers = true;
  }

  checkSubjects(){
    this.checkstudents = false;
    this.checkteachers = false;
    this.checkreport = false;
    this.checksubjects = true;
  }

  checkReport(){
    this.checkstudents = false;
    this.checkteachers = false;
    this.checksubjects = false;
    this.checkreport = true;
  }

  logOut(){
    localStorage.removeItem("isAdmin");
    this.router.navigateByUrl("/login");
  }

}
