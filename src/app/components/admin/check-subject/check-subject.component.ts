import { Component, OnInit } from '@angular/core';
import { SubjectsService } from '../../../services/subjects.service'
import { Subjects } from '../../../entities/subjects';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-check-subject',
  templateUrl: './check-subject.component.html',
  styles: []
})
export class CheckSubjectComponent implements OnInit {

  constructor(private form:FormBuilder, private _subjectService:SubjectsService,  private router:Router,private _toastrService:ToastrService) { }

  subjects:Subjects[];
  subjectForm:FormGroup;

  ngOnInit() {

    if(localStorage.getItem("isStudent") || localStorage.getItem("isTeacher") || localStorage.getItem("isAdmin") == null){
      this.router.navigateByUrl("/home");
    }
    this.createForm()
    this.checkSubjets();
    
  }

  createForm(){
    this.subjectForm = this.form.group({
      id:['',Validators.required],
      title:['',Validators.required],
      description:['',Validators.required],
    });
  }
  addSubject(){
    if(this.subjectForm.valid){
      this.subjects = this._subjectService.addSubjects(this.subjectForm.value);
      this._toastrService.success('La materia fue agregada correctamente','Materia agregada correctamente');
    }else{
      this._toastrService.error('Revisa que los tipos de los campos concuerden con los requeridos','Formulario invalido');
    }
  }
  checkSubjets(){
    this.subjects = JSON.parse(this._subjectService.getAllSubjects());
  }
  
  deleteSubject(id){
    this.subjects = this._subjectService.deleteSubjects(id);
    console.log(id);
  }
}
