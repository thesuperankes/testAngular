import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';  
import { Router } from '@angular/router';

import { StudentsService } from '../../../services/students.service';
import { TeachersService } from '../../../services/teachers.service';
import { SubjectsService } from '../../../services/subjects.service';
import { SubjectPerStudentService } from '../../../services/subject-per-student.service';
import { SubjectPerTeacherService } from '../../../services/subject-per-teacher.service';

import { Students } from '../../../entities/students';
import { Subjects } from '../../../entities/subjects';
import { Teachers } from '../../../entities/teachers';

@Component({
  selector: 'app-generalinfo',
  templateUrl: './generalinfo.component.html',
  styleUrls: []
})
export class GeneralinfoComponent implements OnInit {

  constructor(
    private _studentService:StudentsService,
    private _teacherService:TeachersService,
    private _subjectsPerStudent:SubjectPerStudentService,
    private _subjectsPerTeacher:SubjectPerTeacherService,
    private _subjectsService:SubjectsService,
    private router:Router
  ) { }

  students:Students[];
  teachers:Teachers[];
  subjects:Subjects[];
  subjectsByStudent:any[] = [];
  subjectsByTeacher:any[] = [];
  score:number[];

  ngOnInit() {
    if(localStorage.getItem("isStudent") || localStorage.getItem("isTeacher") || localStorage.getItem("isAdmin") == null){
      this.router.navigateByUrl("/home");
    }

    this.getAllStudents();
    this.getAllTeachers();
    this.getSubjectsByStudent();
    this.getAllSubjects();
    this.getSubjectByTeacher();
    
  }

  getAllStudents(){
    this.students = JSON.parse(this._studentService.getAllStudents());
  }

  getAllTeachers(){
    this.teachers = JSON.parse(this._teacherService.getAllTeachers());
  }

  getAllSubjects(){
    this.subjects = JSON.parse(this._subjectsService.getAllSubjects());
  }

  getSubjectNameById(subjectId:number){
    let subject = this._subjectsService.getSubjectsById(subjectId);
    return subject.title;
  }

  getRandomNumber(){
    return Math.floor(Math.random() * 10) + 1;
  }

  getSubjectsByStudent(){

    for(let item of this.students){
      this.subjectsByStudent.push(this._subjectsPerStudent.getSubjectsByStudentId(item.id));
    }
  }

  getSubjectByTeacher(){
    for(let item of this.teachers){
      this.subjectsByTeacher.push(this._subjectsPerTeacher.getSubjectsByTeacherId(item.id));
    }
    console.log(this.subjectsByTeacher);
  }



  generateReport(){
    var data = document.getElementById('report');

    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('MYPdf.pdf'); // Generated PDF   
    }); 
  }

}

