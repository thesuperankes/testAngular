import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../../../services/students.service';
import { SubjectPerStudentService } from '../../../services/subject-per-student.service';
import { Students } from '../../../entities/students';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { SubjectsService } from '../../../services/subjects.service';

@Component({
  selector: 'app-check-student',
  templateUrl: './check-student.component.html',
  styleUrls: []
})
export class CheckStudentComponent implements OnInit {

  constructor(
    private form: FormBuilder,
    private _studentService:StudentsService,
    private _subjectsByStudent:SubjectPerStudentService,
    private router:Router,
    private _toastrService:ToastrService
    ) { }
  students:Students[];
  score:number[] = [];
  studentForm:FormGroup;


  ngOnInit() {
    if(localStorage.getItem("isStudent") || localStorage.getItem("isTeacher") || localStorage.getItem("isAdmin") == null){
      this.router.navigateByUrl("/home");
    }
    this.students = JSON.parse(this._studentService.getAllStudents());
    this.fillScore();
    this.createForm();
  }

  createForm(){
    this.studentForm = this.form.group({
      id:['',Validators.required],
      name:['',Validators.required],
      lastname:['',Validators.required],
      email:['',Validators.email],
      password:['',Validators.required]
    });
  }

  addStudent(){
    if(this.studentForm.valid){
      this.students = this._studentService.addStudent(this.studentForm.value);
      this._subjectsByStudent.addSubjectsByStudent(this.studentForm.value.id, 2);
      this._toastrService.success('El estudiante fue agregado correctamente','Estudiante agregado correctamente');
    }else{
      this._toastrService.error('Revisa que los tipos de los campos concuerden con los requeridos','Formulario invalido');
    }
  }

  fillScore(){
    for(let items in this.students){
      this.score.push(Math.floor(Math.random() * 10) + 1);
    }
  }
  
}

