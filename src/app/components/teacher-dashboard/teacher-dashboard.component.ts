import { Component, OnInit } from '@angular/core';
import { SubjectsService } from '../../services/subjects.service';
import { SubjectPerTeacherService } from '../../services/subject-per-teacher.service';
import { TeachersService } from '../../services/teachers.service';
import { Subjects } from '../../entities/subjects';
import { Teachers } from '../../entities/teachers';


@Component({
  selector: 'app-teacher-dashboard',
  templateUrl: './teacher-dashboard.component.html',
  styleUrls: []
})
export class TeacherDashboardComponent implements OnInit {

  constructor(private _teacherService :TeachersService,private _subjectService:SubjectsService, private _subjectPerTeacher:SubjectPerTeacherService) { }

  subjectsId:number[];
  teacherSubjects:Subjects[] = [];
  teacher:Teachers;

  ngOnInit() {
    this.teacher = JSON.parse(localStorage.getItem("Teacher"));
    console.log(this.teacher.id);
    this.subjectsId = this._subjectPerTeacher.getSubjectsByTeacherId(this.teacher.id).subjectid;
    
    for(let item of this.subjectsId){
      this.teacherSubjects.push(this._subjectService.getSubjectsById(item));
    }
  }

}
