import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { StudentDashboardComponent } from './components/student-dashboard/student-dashboard.component';
import { TeacherDashboardComponent } from './components/teacher-dashboard/teacher-dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { CheckStudentComponent } from './components/admin/check-student/check-student.component'
import { CheckTeacherComponent } from './components/admin/check-teacher/check-teacher.component'
import { GeneralinfoComponent } from './components/admin/generalinfo/generalinfo.component'

const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'home', component: HomeComponent },
    { path: 'studentDashboard', component: StudentDashboardComponent },
    { path: 'teacherDashboard', component: TeacherDashboardComponent },
    { path: 'login', component: LoginComponent },
    { path: 'checkStudentDashboard', component: CheckStudentComponent },
    { path: 'checkTeacherDashboard', component: CheckTeacherComponent },
    { path: 'generateReport', component: GeneralinfoComponent },
    { path: 'admin', component: AdminComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const app_routing = RouterModule.forRoot(routes, {useHash:true});