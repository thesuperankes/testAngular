# Herramienta para la administracion de colegio Demo

Proyecto creado en angular 6.

## Build

Para lograr una correcta compilacion se debe tener en cuenta que el proyecto usa diferentes librerias, y tal vez sea necesario ejecutar su debida instalacion:

```
npm install ngx-toastr --save
npm install jspdf --save
npm install html2canvas --save
npm install bootstrap@4.0.0-alpha.6 
```


despues puedes arrancarlo con `ng serve` para un servidor de pruebas. Entra a `http://localhost:4200/`.

## Datos de prueba

Al ser un demo muchos de los datos son de prueba y por ende generara varios errores.

Cuidados:
Al momento de crear un estudiante, profesor, o materia si despues vas a generar el reporte es necesario cargar la pagina ya que esta informacion no se esta almacenando en nigun lugar, entonces generará un conflicto

##Credenciales

```
Estudiante:
Correo: lopezavellaneda@hotmail.es
Contraseña: 123456

Profesor
Correo: elprofe@miraflores.com
Contraseña 123456

Admin
Correo: admin@admin.com
Contraseña 123456
```

## Autor

* **Andres David Lopez**